package com.inventain.booking.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inventain.booking.config.AppConfig;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MeetingDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
    private static final DateTimeFormatter df
            = DateTimeFormatter.ofPattern(AppConfig.getProperty(AppConfig.MEETING_DATE_TIME_FORMAT));

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return LocalDateTime.parse(jsonParser.getText(), df);
    }
}
