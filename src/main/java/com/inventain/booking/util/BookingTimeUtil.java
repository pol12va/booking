package com.inventain.booking.util;

import com.inventain.booking.representation.BookingRequestItem;

import java.time.LocalTime;

public class BookingTimeUtil {

    public static boolean isBookingInPeriod(LocalTime officeOpens,
                                            LocalTime officeCloses,
                                            BookingRequestItem item) {
        LocalTime meetingStarts = item.getMeetingStarts().toLocalTime();
        LocalTime meetingEnds = item.getMeetingStarts()
                .plusHours(item.getDuration()).toLocalTime();

        return meetingStarts.equals(officeOpens)
                || meetingStarts.isAfter(officeOpens)
                && meetingEnds.equals(officeCloses)
                || meetingEnds.isBefore(officeCloses);
    }

}
