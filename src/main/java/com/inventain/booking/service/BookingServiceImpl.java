package com.inventain.booking.service;

import com.inventain.booking.processor.BookingRequestProcessorManager;
import com.inventain.booking.processor.impl.OverlappedBookingItemsProcessor;
import com.inventain.booking.processor.impl.TimeBookingItemsProcessor;
import com.inventain.booking.processor.impl.UniqueBookingItemsProcessor;
import com.inventain.booking.representation.BookingRequest;
import com.inventain.booking.representation.BookingResult;

import com.inventain.booking.representation.mapper.RequestToResultMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class BookingServiceImpl implements BookingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingServiceImpl.class);

    @Override
    public List<BookingResult> process(BookingRequest request) {

        LOGGER.debug("Request processing started");

        Collections.sort(request.getRequests(), (i1, i2) -> i1.getSubmissionTime().compareTo(i2.getSubmissionTime()));

        BookingRequestProcessorManager manager
                = new BookingRequestProcessorManager(request);

        manager.addProcessor(new TimeBookingItemsProcessor());
        manager.addProcessor(new UniqueBookingItemsProcessor());
        manager.addProcessor(new OverlappedBookingItemsProcessor());

        manager.execute();

        LOGGER.debug("Request processing completed");

        return RequestToResultMapper.map(request.getRequests());

    }

}
