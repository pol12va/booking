package com.inventain.booking.processor;

import com.inventain.booking.representation.BookingRequest;

public interface BookingRequestProcessor {

    void process(BookingRequest bookingRequest);

}
