package com.inventain.booking.service;

import com.inventain.booking.AbstractBookingTest;
import com.inventain.booking.AppTestConfig;
import com.inventain.booking.representation.BookingResult;
import com.inventain.booking.representation.BookingResultItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppTestConfig.class,
        loader = AnnotationConfigContextLoader.class)
public class BookingServiceTest extends AbstractBookingTest {

    @Autowired
    private BookingService bookingService;

    @Before
    public void setUp() {
        init();
    }

    @Test
    public void testProcessBookingRequest() {
        List<BookingResult> result = bookingService.process(bookingRequest);
        Stream<BookingResultItem> itemsStream = result.stream()
                .map(BookingResult::getItems)
                .flatMap(List::stream);

        StringBuilder ids = new StringBuilder();

        itemsStream.forEach(item -> ids.append(item.getEmployeeId()).append(","));
        ids.deleteCharAt(ids.length() - 1);

        assertEquals(2, result.size());
        assertEquals("EMP002,EMP003,EMP004", ids.toString());
    }

}
