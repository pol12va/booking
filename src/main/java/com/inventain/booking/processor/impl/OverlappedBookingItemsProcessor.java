package com.inventain.booking.processor.impl;

import com.inventain.booking.processor.BookingRequestProcessor;
import com.inventain.booking.representation.BookingRequest;
import com.inventain.booking.representation.BookingRequestItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class OverlappedBookingItemsProcessor implements BookingRequestProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(OverlappedBookingItemsProcessor.class);

    @Override
    public void process(BookingRequest bookingRequest) {

        if (bookingRequest == null) {
            LOGGER.error("Booking request parameter is null");
            throw new IllegalArgumentException("Request can't be null");
        }

        List<BookingRequestItem> items = bookingRequest.getRequests();
        List<BookingRequestItem> filteredBookings = new ArrayList<>();
        Set<BookingRequestItem> allOverlappedBookings = new LinkedHashSet<>();

        LOGGER.debug("Bookings size before filtering overlapped bookings" + items.size());

        for (int i = 0; i < items.size() - 1; i++) {
            if (!allOverlappedBookings.contains(items.get(i))) {
                List<BookingRequestItem> overlappedBookings = new ArrayList<>();
                for (int j = i + 1; j < items.size(); j++) {
                    if (items.get(i).isOverlaps(items.get(j))) {
                        overlappedBookings.add(items.get(i));
                        overlappedBookings.add(items.get(j));
                        allOverlappedBookings.addAll(overlappedBookings);
                    }
                }

                if (!overlappedBookings.isEmpty()) {
                    filteredBookings.add(overlappedBookings.get(0));
                } else if (!allOverlappedBookings.contains(items.get(i))) {
                    filteredBookings.add(items.get(i));
                }
            }
        }
        if (!allOverlappedBookings.contains(items.get(items.size() - 1))) {
            filteredBookings.add(items.get(items.size() - 1));
        }

        LOGGER.debug("Bookings size after filtering overlapped bookings: " + filteredBookings.size());

        bookingRequest.setRequests(filteredBookings);

    }
}
