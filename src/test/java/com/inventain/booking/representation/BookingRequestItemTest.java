package com.inventain.booking.representation;

import com.inventain.booking.config.AppConfig;
import org.junit.Test;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BookingRequestItemTest {

    @Test
    public void testIsOverlaps() throws ParseException {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(AppConfig.getProperty(AppConfig.MEETING_DATE_TIME_FORMAT));
        LocalDateTime d1 = LocalDateTime.parse("2011-03-22 16:00", df);
        LocalDateTime d2 = LocalDateTime.parse("2011-03-22 14:00", df);
        LocalDateTime d3 = LocalDateTime.parse("2011-03-21 09:00", df);
        LocalDateTime d5 = LocalDateTime.parse("2011-03-21 10:00", df);

        BookingRequestItem i1 = new BookingRequestItem();
        BookingRequestItem i2 = new BookingRequestItem();
        BookingRequestItem i3 = new BookingRequestItem();
        BookingRequestItem i4 = new BookingRequestItem();
        BookingRequestItem i5 = new BookingRequestItem();

        i1.setMeetingStarts(d1);
        i2.setMeetingStarts(d2);
        i3.setMeetingStarts(d3);
        i4.setMeetingStarts(d3);
        i5.setMeetingStarts(d5);
        i1.setDuration(1);
        i2.setDuration(2);
        i3.setDuration(2);
        i4.setDuration(2);
        i5.setDuration(3);

        assertFalse(i1.isOverlaps(i2));
        assertTrue(i3.isOverlaps(i4));
        assertTrue(i4.isOverlaps(i5));
    }
}
