package com.inventain.booking.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inventain.booking.AbstractBookingTest;
import com.inventain.booking.processor.impl.OverlappedBookingItemsProcessor;
import com.inventain.booking.processor.impl.TimeBookingItemsProcessor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OverlappedBookingItemsProcessorTest extends AbstractBookingTest {

    @Before
    public void setUp() {
        init();
    }

    @Test
    public void testOverlappedBookingsProcessing() {
        BookingRequestProcessorManager manager
                = new BookingRequestProcessorManager(bookingRequest);

        manager.addProcessor(new OverlappedBookingItemsProcessor());

        manager.execute();

        StringBuilder ids = new StringBuilder("");

        bookingRequest.getRequests()
                .forEach((item) -> ids.append(item.getEmployeeId()).append(","));

        assertEquals("EMP001,EMP003,EMP004,EMP005,EMP006,", ids.toString());
    }

}
