package com.inventain.booking.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inventain.booking.AbstractBookingTest;
import com.inventain.booking.processor.impl.UniqueBookingItemsProcessor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UniqueBookingItemsProcessorTest extends AbstractBookingTest {

    @Before
    public void setUp() {
        init();
    }

    @Test
    public void testProcessing() {
        BookingRequestProcessorManager processorManager
                = new BookingRequestProcessorManager(bookingRequest);

        processorManager.addProcessor(new UniqueBookingItemsProcessor());

        processorManager.execute();

        StringBuilder ids = new StringBuilder("");

        bookingRequest.getRequests()
                .forEach((item) -> ids.append(item.getEmployeeId()).append(","));

        assertEquals("EMP001,EMP002,EMP003,EMP004,EMP005,", ids.toString());
    }

}
