package com.inventain.booking.representation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.inventain.booking.util.TimeDeserializer;

import java.time.LocalTime;
import java.util.List;

public class BookingRequest {
    @JsonDeserialize(using = TimeDeserializer.class)
    private LocalTime officeOpensTime;
    @JsonDeserialize(using = TimeDeserializer.class)
    private LocalTime officeClosesTime;

    private List<BookingRequestItem> requests;

    public LocalTime getOfficeOpensTime() {
        return officeOpensTime;
    }

    public void setOfficeOpensTime(LocalTime officeOpensTime) {
        this.officeOpensTime = officeOpensTime;
    }

    public LocalTime getOfficeClosesTime() {
        return officeClosesTime;
    }

    public void setOfficeClosesTime(LocalTime officeClosesTime) {
        this.officeClosesTime = officeClosesTime;
    }

    public List<BookingRequestItem> getRequests() {
        return requests;
    }

    public void setRequests(List<BookingRequestItem> requests) {
        this.requests = requests;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(officeOpensTime).append(' ')
                .append(officeClosesTime).append('\n');

        requests.forEach(request -> result.append(request.toString()));

        return result.toString();
    }
}
