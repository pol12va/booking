package com.inventain.booking.representation;

public class BookingResultItem {
    private String employeeId;
    private String startTime;
    private String endTime;

    public BookingResultItem() {}

    public BookingResultItem(BookingRequestItem requestItem) {
        this.employeeId = requestItem.getEmployeeId();
        this.startTime = requestItem.getMeetingStarts().toLocalTime().toString();
        this.endTime = requestItem.getMeetingStarts()
                .plusHours(requestItem.getDuration()).toLocalTime().toString();
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
