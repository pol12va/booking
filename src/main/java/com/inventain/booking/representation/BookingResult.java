package com.inventain.booking.representation;

import java.util.ArrayList;
import java.util.List;

public class BookingResult {
    private String meetingDate;
    private List<BookingResultItem> items = new ArrayList<>();

    public String getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(String meetingDate) {
        this.meetingDate = meetingDate;
    }

    public List<BookingResultItem> getItems() {
        return items;
    }

    public void setItems(List<BookingResultItem> items) {
        this.items = items;
    }
}
