package com.inventain.booking.service;

import com.inventain.booking.representation.BookingRequest;
import com.inventain.booking.representation.BookingResult;

import java.util.List;

public interface BookingService {
    List<BookingResult> process(BookingRequest request);
}
