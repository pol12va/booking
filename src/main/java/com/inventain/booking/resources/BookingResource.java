package com.inventain.booking.resources;

import com.inventain.booking.representation.BookingRequest;
import com.inventain.booking.representation.BookingResult;
import com.inventain.booking.service.BookingService;
import org.restlet.ext.jackson.JacksonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StreamRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


public class BookingResource extends ServerResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingResource.class);

    @Autowired
    private BookingService bookingService;

    @Post ("json")
    public Representation store(BookingRequest requests) {
        LOGGER.debug('\n' + requests.toString());

        List<BookingResult> results = bookingService.process(requests);

        return new JacksonRepresentation<>(results);
    }
}
