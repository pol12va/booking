package com.inventain.booking;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inventain.booking.representation.BookingRequest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public abstract class AbstractBookingTest {

    protected BookingRequest bookingRequest;

    public void init() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            bookingRequest = mapper.readValue(
                    new File(getClass().getClassLoader().getResource("booking.json").toURI()),
                    BookingRequest.class);
        } catch (URISyntaxException | IOException e) {
            throw new RuntimeException("File reading failed");
        }

    }

}
