package com.inventain.booking;

import com.inventain.booking.resources.BookingResource;
import com.inventain.booking.service.BookingService;
import com.inventain.booking.service.BookingServiceImpl;
import org.restlet.ext.spring.SpringComponent;
import org.restlet.ext.spring.SpringFinder;
import org.restlet.ext.spring.SpringRouter;
import org.restlet.ext.spring.SpringServer;
import org.restlet.resource.ServerResource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class AppConfig {

    @Bean
    public BookingService bookingService() {
        return new BookingServiceImpl();
    }

    @Bean
    @Scope("prototype")
    public BookingResource bookingResource() {
        return new BookingResource();
    }

    @Bean
    public SpringComponent server() {
        SpringComponent server = new SpringComponent();
        SpringServer springServer = new SpringServer("http", 8080);

        server.setServer(springServer);
        server.setDefaultTarget(root());

        return server;
    }

    @Bean
    public SpringRouter root() {
        SpringRouter router = new SpringRouter();
        Map<String, Object> attachments = new HashMap<>();

        attachments.put("/bookings", new SpringFinder() {
            @Override
            public ServerResource create() {
                return bookingResource();
            }
        });

        router.setAttachments(attachments);

        return router;
    }

}
