package com.inventain.booking.representation.mapper;

import com.inventain.booking.representation.BookingRequestItem;
import com.inventain.booking.representation.BookingResult;
import com.inventain.booking.representation.BookingResultItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class RequestToResultMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestToResultMapper.class);

    public static List<BookingResult> map(List<BookingRequestItem> requestItems) {

        if (requestItems == null) {
            LOGGER.error("Request list parameter is null");
            throw new IllegalArgumentException("Request list can't be null");
        }

        List<BookingResult> output = new ArrayList<>();
        Set<LocalDate> dates = new TreeSet<>(LocalDate::compareTo);

        requestItems.stream()
                .map(item -> item.getMeetingStarts().toLocalDate())
                .forEach(dates::add);

        dates.forEach(date -> {
            BookingResult bookingResult = new BookingResult();
            bookingResult.setMeetingDate(date.toString());
            List<BookingResultItem> singeDateItems = requestItems.stream()
                    .filter(resultItem -> resultItem.getMeetingStarts().toLocalDate().equals(date))
                    .map(BookingResultItem::new)
                    .collect(Collectors.toList());

            bookingResult.getItems().addAll(singeDateItems);
            output.add(bookingResult);
        });

        LOGGER.debug("Result item size after processing bookings: " + output.size());

        return output;
    }

}
