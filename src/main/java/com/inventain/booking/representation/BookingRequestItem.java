package com.inventain.booking.representation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.inventain.booking.config.AppConfig;
import com.inventain.booking.util.MeetingDateTimeDeserializer;
import com.inventain.booking.util.SubmissionDateTimeDeserializer;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class BookingRequestItem {
    private String employeeId;
    private Integer duration;
    @JsonDeserialize(using = SubmissionDateTimeDeserializer.class)
    private LocalDateTime submissionTime;
    @JsonDeserialize(using = MeetingDateTimeDeserializer.class)
    private LocalDateTime meetingStarts;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public LocalDateTime getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(LocalDateTime submissionTime) {
        this.submissionTime = submissionTime;
    }

    public LocalDateTime getMeetingStarts() {
        return meetingStarts;
    }

    public void setMeetingStarts(LocalDateTime meetingStarts) {
        this.meetingStarts = meetingStarts;
    }

    public boolean isOverlaps(BookingRequestItem item) {
        return isSameDate(item) && isTimeOverlaps(item);
    }

    public boolean isSameDate(BookingRequestItem item) {
        return meetingStarts.toLocalDate().equals(item.getMeetingStarts().toLocalDate());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BookingRequestItem item = (BookingRequestItem) o;

        return Objects.equals(employeeId, item.employeeId) &&
                Objects.equals(meetingStarts, item.meetingStarts) &&
                Objects.equals(duration, item.duration) &&
                Objects.equals(submissionTime, item.submissionTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeId, meetingStarts, duration, submissionTime);
    }

    @Override
    public String toString() {
        DateTimeFormatter submissionDf
                = DateTimeFormatter.ofPattern(AppConfig.getProperty(AppConfig.SUBMISSION_DATE_TIME_FORMAT));
        DateTimeFormatter meetingDf
                = DateTimeFormatter.ofPattern(AppConfig.getProperty(AppConfig.MEETING_DATE_TIME_FORMAT));

        return submissionTime.format(submissionDf) + " " + employeeId + '\n'
                + meetingStarts.format(meetingDf) + " " + duration + '\n';
    }

    private boolean isTimeOverlaps(BookingRequestItem item) {
        LocalDateTime meetingEnds = meetingStarts.plusHours(duration);
        LocalDateTime otherMeetingEnds = item.meetingStarts.plusHours(item.duration);

        return meetingStarts.equals(item.meetingStarts)
                || meetingEnds.equals(otherMeetingEnds)
                || meetingStarts.isAfter(item.meetingStarts) && meetingEnds.isBefore(otherMeetingEnds)
                || meetingStarts.isBefore(item.meetingStarts) && meetingEnds.isAfter(item.meetingStarts)
                || item.meetingStarts.isAfter(meetingStarts) && otherMeetingEnds.isBefore(meetingEnds)
                || item.meetingStarts.isBefore(meetingStarts) && otherMeetingEnds.isAfter(meetingStarts)
                || meetingStarts.isBefore(item.meetingStarts) && meetingEnds.isAfter(otherMeetingEnds)
                || item.meetingStarts.isBefore(meetingStarts) && otherMeetingEnds.isAfter(meetingEnds);
    }
}
