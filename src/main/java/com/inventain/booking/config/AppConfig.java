package com.inventain.booking.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppConfig {
    public static final String TIME_FORMAT = "time.format";
    public static final String SUBMISSION_DATE_TIME_FORMAT = "submission.date.time.format";
    public static final String MEETING_DATE_TIME_FORMAT = "meeting.date.time.format";

    private static final Logger LOGGER = LoggerFactory.getLogger(AppConfig.class);

    private static final String CONFIGURATION_FILE = "app.config.properties";

    private static Properties props = new Properties();

    private static final AppConfig INSTANCE = new AppConfig();

    private AppConfig() {
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream(CONFIGURATION_FILE);

            if (is != null) {
                props.load(is);
            }
        } catch (IOException e) {
            LOGGER.error("Can't load application settings from file");
        }
    }

    public static String getProperty(String key) {
        return props.getProperty(key) != null ? props.getProperty(key) : "";
    }

}
