package com.inventain.booking.processor;

import com.inventain.booking.representation.BookingRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BookingRequestProcessorManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingRequestProcessorManager.class);

    private BookingRequest bookingRequest;
    private List<BookingRequestProcessor> processors = new ArrayList<>();

    public BookingRequestProcessorManager(BookingRequest bookingRequest) {
        if (bookingRequest == null) {
            LOGGER.error("Booking request parameter is null");
            throw new IllegalArgumentException("Request can't be null");
        }

        this.bookingRequest = bookingRequest;
    }

    public void addProcessor(BookingRequestProcessor processor) {
        processors.add(processor);
    }

    public void execute() {

        processors.forEach(processor -> processor.process(bookingRequest));

    }

}
