package com.inventain.booking.processor.impl;

import com.inventain.booking.processor.BookingRequestProcessor;
import com.inventain.booking.representation.BookingRequest;
import com.inventain.booking.representation.BookingRequestItem;
import com.inventain.booking.util.BookingTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TimeBookingItemsProcessor implements BookingRequestProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeBookingItemsProcessor.class);

    @Override
    public void process(BookingRequest bookingRequest) {

        if (bookingRequest == null) {
            LOGGER.error("Booking request parameter is null");
            throw new IllegalArgumentException("Booking request can't be null");
        }

        Predicate<BookingRequestItem> isBookingInPeriod = (item) ->
                BookingTimeUtil.isBookingInPeriod(
                        bookingRequest.getOfficeOpensTime(),
                        bookingRequest.getOfficeClosesTime(),
                        item
        );

        List<BookingRequestItem> afterProcessing = bookingRequest.getRequests().stream()
                .filter(isBookingInPeriod)
                .collect(Collectors.toList());

        bookingRequest.setRequests(afterProcessing);

    }
}
