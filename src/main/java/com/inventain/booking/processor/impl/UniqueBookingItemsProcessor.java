package com.inventain.booking.processor.impl;

import com.inventain.booking.processor.BookingRequestProcessor;
import com.inventain.booking.representation.BookingRequest;
import com.inventain.booking.representation.BookingRequestItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class UniqueBookingItemsProcessor implements BookingRequestProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(UniqueBookingItemsProcessor.class);

    @Override
    public void process(BookingRequest bookingRequest) {

        if (bookingRequest == null) {
            LOGGER.error("Booking request parameter is null");
            throw new IllegalArgumentException("Request can't be null");
        }

        List<BookingRequestItem> items = bookingRequest.getRequests();
        LOGGER.debug("Removing requests with same submission time");

        List<BookingRequestItem> filteredItems = new ArrayList<>();
        Stream<LocalDateTime> uniqueSubmissionTimes = items.stream()
                .map(BookingRequestItem::getSubmissionTime)
                .distinct();

        uniqueSubmissionTimes.forEach(submissionTime ->
                filteredItems.add(items.stream()
                        .filter(item -> item.getSubmissionTime().equals(submissionTime))
                        .findFirst()
                        .orElseThrow(RuntimeException::new)));

        bookingRequest.setRequests(filteredItems);

    }
}
