package com.inventain.booking.representation;

import com.inventain.booking.AbstractBookingTest;
import com.inventain.booking.representation.mapper.RequestToResultMapper;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class RequestToResultMapperTest extends AbstractBookingTest {

    @Before
    public void setUp() {
        init();
    }

    @Test
    public void testMapping() {
        List<BookingResult> results
                = RequestToResultMapper.map(bookingRequest.getRequests());

        assertEquals(3, results.size());
    }

}
