package com.inventain.booking.util;

import com.inventain.booking.AbstractBookingTest;
import com.inventain.booking.representation.BookingRequestItem;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BookingTimeUtilTest extends AbstractBookingTest {

    @Before
    public void setUp() {
        init();
    }

    @Test
    public void testIsBookingInPeriod() {
        LocalTime opensTime = bookingRequest.getOfficeOpensTime();
        LocalTime closesTime = bookingRequest.getOfficeClosesTime();
        BookingRequestItem goodItem = bookingRequest.getRequests().get(0);
        BookingRequestItem badItem = bookingRequest.getRequests().get(4);

        assertTrue(BookingTimeUtil.isBookingInPeriod(
                opensTime, closesTime, goodItem
        ));
        assertFalse(BookingTimeUtil.isBookingInPeriod(
                opensTime, closesTime, badItem
        ));
    }

}
