package com.inventain.booking;

import org.restlet.*;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BookingApplication extends Application {

    public static void main(String[] args) throws Exception {
        SLF4JBridgeHandler.install();

        ApplicationContext context
                = new AnnotationConfigApplicationContext(AppConfig.class);

        Component c = context.getBean("server", Component.class);
        c.start();
    }
}
