package com.inventain.booking;

import com.inventain.booking.service.BookingService;
import com.inventain.booking.service.BookingServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppTestConfig {

    @Bean
    public BookingService bookingService() {
        return new BookingServiceImpl();
    }

}
